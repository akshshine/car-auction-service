const createError = require('http-errors');

const { getEndedAuctions } = require('../lib/getEndDateAuctions');
const { closeAuction } = require('../lib/closeAuction');

async function processAuctions(event, context) {
  try {
    // Array of Auctions to be closed
    const auctionsToClose = await getEndedAuctions();

    // Array of promises return by closeAuction for each item in auctionsToClose
    const closePromises = auctionsToClose.map((auction) => closeAuction(auction));

    // It will wait for all these promises from closePromises to be resolved instead of waiting for all promises to resolved
    await Promise.all(closePromises);

    return {
      closed: closePromises.length
    };
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = processAuctions;
