const AWS = require('aws-sdk');

const middy = require('@middy/core');
const httpJsonBodyParser = require('@middy/http-json-body-parser');
const httpEventNormalizer = require('@middy/http-event-normalizer');
const httpErrorHandler = require('@middy/http-error-handler');
const validator = require('@middy/validator');
const { transpileSchema } = require('@middy/validator/transpile');

const createError = require('http-errors');

const dynamoDB = new AWS.DynamoDB.DocumentClient();

const getAllAuctionsSchema = require('../lib/schemas/getAllAuctionsSchema');

const options = {
  eventSchema: getAllAuctionsSchema ? transpileSchema(getAllAuctionsSchema) : undefined,
  ajvOptions: {
    useDefaults: true,
    strict: false
  }
};

async function getAllAuctions(event, context) {
  let auctions;
  const { status } = event.queryStringParameters;

  const params = {
    TableName: process.env.AUCTIONS_TABLE_NAME,
    IndexName: 'statusAndEndDate',
    KeyConditionExpression: '#status = :status',
    ExpressionAttributeValues: {
      ':status': status
    },
    // Expression having resevered word status in query hence we need to provide ExpressionAttributeNames for it
    ExpressionAttributeNames: {
      '#status': 'status'
    }
  };

  // Commenting try catch block so that schema validation will work
  //try {
    const result = await dynamoDB.query(params).promise();

    auctions = result.Items;
  // } catch (error) {
  //   console.log('**********************error happened in getAllAuctions:'+ error);
  //   throw new createError.InternalServerError(error);
  // }

  return {
    statusCode: 200,
    body: JSON.stringify(auctions)
  };
}

export const handler = middy(getAllAuctions)
  .use(httpJsonBodyParser())
  .use(validator(options))
  .use(httpErrorHandler())
  .use(httpEventNormalizer());
