const AWS = require('aws-sdk');

const middy = require('@middy/core');
const httpJsonBodyParser = require('@middy/http-json-body-parser');
const httpEventNormalizer = require('@middy/http-event-normalizer');
const httpErrorHandler = require('@middy/http-error-handler');
const validator = require('@middy/validator');
const { transpileSchema } = require('@middy/validator/transpile');

const { getAuctionById } = require('./getAuction');

const createError = require('http-errors');

const placeBidSchema = require('../lib/schemas/placeBidSchema');

const options = {
  eventSchema: placeBidSchema ? transpileSchema(placeBidSchema) : undefined,
  ajvOptions: {
    useDefaults: false,
    strict: true,
  }
};

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function placeBid(event, context) {
  const { id } = event.pathParameters;
  const { amount } = event.body;
  const { email } = event.requestContext.authorizer;

  const auction = await getAuctionById(id);

  //Bid identity validation
  if (email === auction.seller){
    throw new createError.Forbidden('You can not bid on your own auctions!');
  }

  // Avoid double bidding
  if (email === auction.highestBid.bidder){
    throw new createError.Forbidden('You are already hightest bidder');
  }

  // Auction status validation
  if (auction.status !== 'OPEN') {
    throw new createError.Forbidden('You cannot bid on closed auctions!');
  }

  // Bid amount validation
  if (amount <= auction.highestBid.amount) {
    throw new createError.Forbidden(`Your bid must be higher than ${auction.highestBid.amount}!`);
  }

  const params = {
    TableName: process.env.AUCTIONS_TABLE_NAME,
    Key: { id },
    UpdateExpression: 'set highestBid.amount = :amount, highestBid.bidder = :bidder',
    ExpressionAttributeValues: {
      ':amount': amount,
      ':bidder': email,
    },
    ReturnValues: 'ALL_NEW'
  };

  let updatedAuction;
  try {
    const result = await dynamoDB.update(params).promise();
    updatedAuction = result.Attributes;
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(updatedAuction)
  };
}

export const handler = middy(placeBid)
    .use(httpJsonBodyParser())
    .use(validator(options))
    .use(httpErrorHandler())
    .use(httpEventNormalizer());
