const { v4: uuid } = require('uuid');
const AWS = require('aws-sdk');

const middy = require('@middy/core');
const httpJsonBodyParser = require('@middy/http-json-body-parser');
const httpEventNormalizer = require('@middy/http-event-normalizer');
const httpErrorHandler = require('@middy/http-error-handler');
const validator = require('@middy/validator');
const { transpileSchema } = require('@middy/validator/transpile');

const createError = require('http-errors');

const createAuctionSchema = require('../lib/schemas/createAuctionSchema');

const options = {
  eventSchema: createAuctionSchema ? transpileSchema(createAuctionSchema) : undefined,
  ajvOptions: {
    useDefaults: false,
    strict: true,
  }
};

const dynamoDB = new AWS.DynamoDB.DocumentClient();

async function createAuction(event, context) {
  const { title } = event.body;
  const { email } = event.requestContext.authorizer;
  const now = new Date();
  const endDate = new Date();
  endDate.setHours(now.getHours() + 1);

  const auction = {
    id: uuid(),
    title,
    status: 'OPEN',
    createdAt: now.toISOString(),
    endingAt: endDate.toISOString(),
    highestBid: {
      amount: 0
    },
    seller: email,
  };

  try {
    await dynamoDB
      .put({
        TableName: process.env.AUCTIONS_TABLE_NAME,
        Item: auction
      })
      .promise();
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }

  return {
    statusCode: 201,
    body: JSON.stringify(auction)
  };
}

export const handler = middy(createAuction)
  .use(httpJsonBodyParser()) // It will automatically parse our stringified event body
  .use(validator(options))
  .use(httpErrorHandler()) // It can help us make our handling error process smooth, easy and clean by working with http-errors package
  .use(httpEventNormalizer()); // It will automatically adjust the API gateway object to prevent us from accidently a non exiting object when trying to access pathParameters or query parameters
