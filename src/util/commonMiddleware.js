// const middy = require('@middy/core');
// const httpJsonBodyParser = require('@middy/http-json-body-parser');
// const httpEventNormalizer = require('@middy/http-event-normalizer');
// const httpErrorHandler = require('@middy/http-error-handler');
// const httpResponseSerializer = require('@middy/http-response-serializer');
// const validator = require('@middy/validator');
// const { transpileSchema } = require('@middy/validator/transpile');

// const defaults = {
//   ajvOptions: { useDefaults: true },
//   noPII: true,
//   httpApi: true
// };

// const commonMiddleware = function (baseHandler, opts = {}) {
//   console.log('*******************************Inside commonMiddleware');
//   let middleware = middy(baseHandler)
//     .use(httpEventNormalizer())
//     .use(httpJsonBodyParser())
//     .use(httpErrorHandler())
//     .use(
//       httpResponseSerializer({
//         // after, onError
//         serializers: [
//           {
//             regex: /^application\/json$/,
//             serializer: ({ body }) => JSON.stringify(body)
//           }
//         ],
//         defaultContentType: 'application/json'
//       })
//     );

//   const { inputSchema, outputSchema } = { ...defaults, ...opts };
//   if (inputSchema || outputSchema) {
//     const options = {
//       eventSchema: inputSchema ? transpileSchema(inputSchema) : undefined,
//       responseSchema: outputSchema ? transpileSchema(outputSchema) : undefined
//     };
//     middleware = middleware.use(validator(options));
//   }

//   return middleware;
// };

// export default commonMiddleware;
