const schema = {
    type: 'object',
    properties: {
        body: {
            type: 'object',
            properties: {
                title: {
                    type: 'string'
                }
            },
            //required: ['title'],
        }
    },
    required: ['title'],
}


export default schema;