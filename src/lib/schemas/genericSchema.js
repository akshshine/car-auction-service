const auctionItemStatus = {
  type: 'string',
  enum: ['OPEN', 'CLOSED'],
  default: 'OPEN',
};