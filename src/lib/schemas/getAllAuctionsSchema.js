const genericDefinitions = require('./genericSchema');

const schema = {
  type: 'object',
  properties: {
    queryStringParameters: {
      type: 'Object',
      properties: {
        status: genericDefinitions.auctionItemStatus,
      },
      required: ['status'],
    }
  },
};

export default schema;
