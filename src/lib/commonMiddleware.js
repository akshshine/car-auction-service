const middy = require('@middy/core');
const httpJsonBodyParser = require('@middy/http-json-body-parser');
const httpEventNormalizer = require('@middy/http-event-normalizer');
const httpErrorHandler = require('@middy/http-error-handler');

export default handler => {
  console.log('middy handler callled =========>'+handler)
  middy(handler)
  .use([
    httpJsonBodyParser(), 
    httpEventNormalizer(), 
    httpErrorHandler()
  ])
};

// httpJsonBodyParser() => It will automatically parse our stringified event body
// httpEventNormalizer() => It will automatically adjust the API gateway object to prevent us from accidently a non exiting object when trying to access pathParameters or query parameters
// httpErrorHandler() => It can help us make our handling error process smooth, easy and clean by working with http-errors package
